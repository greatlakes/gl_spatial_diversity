# gl_spatial_diversity

Code and data describing the spatial diversity of microorganisms in the Laurentian Great Lakes as presented in:
Microbial communities of the Laurentian Great Lakes reflect connectivity and local biogeochemistry
https://sfamjournals.onlinelibrary.wiley.com/doi/abs/10.1111/1462-2920.14862

Please contact Sara Paver (spaver@uchicago.edu) with any questions

File structure:
code/ R scripts to produce corresponding figures
data/ data needed to run R scripts, fasta files with representative sequences for each oligotype ("node representatives")
mothur_logs/ detailed logs of the pipeline used to analyze sequences in mothur
results/

If you create a "repos" directory in your home directory, you should be able to clone this repository into your repos directory and run all of the code